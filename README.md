<h1 align="center">
    Greetings to all, <a href="https://t.me/ArtLkv_DV" target="_blank">『Arthur』</a> is with you 👋 
</h1>

<div align="left">
<h3 align="center">
I won't pull the cat by the balls. My name is Arthur, I'm 18. I'm a fan of FOSS, Linux and Anime. My dream is to start contributing to Open Source.
</h3>
</div>

# About me... <img src="https://media.giphy.com/media/mGcNjsfWAjY5AEZNw6/giphy.gif" width="50">

```ts
const name: string = "Arthur";
const surname: string = "Lokhov";

const challenge: string = "I am doing the #100DaysOfCode challenge focused on JS and his tools.";

let codeSkills: string[] = [
    "Markdown, Html, Css",
    "JavaScript",
    "SQL"
];

let toolsSkills: string[] = [
    "git",
    "linux administration, btw I use Arch",
    "DBeaver",
    "Visual Studio Code"
];

let databaseKnowledge: string[] = [
    "PostgreSQL",
    "MongoDB"
];

let WhatIWantToStudy: string[] = [
    "TypeScript",
    "Webpack/Vite",
    "Prisma ORM",
    "Electron",
    "React/NextJS",
    "NodeJS/NestJS"
    "Rust/C/C++"
];
```
</div>